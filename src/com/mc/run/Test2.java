package com.mc.run;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;

/**
 * Created by LCF on 2016/1/28.
 */
public class Test2 {
    public static void main(String[] args) throws IOException, ParseException {
        /*region 创建索引*/
        //创建分词器，此处可以替换为符合中文要求的分词器。
        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        //根据分词器创建 IndexWriter的配置文件。
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        //设置索引文件存放的目录。
        Directory directory = FSDirectory.open(new File("/tmp/text-index"));
        //创建 IndexWriter
        IndexWriter indexWriter = new IndexWriter(directory, config);
        //类似数据库中的一行
        Document document1 = new Document();
        Document document2 = new Document();
        String text = "This is the text to be indexed and text.";
        String text2 = "I is Tom,Error,Day Day up index and text learn";
        //Field类似数据库中的一列。
        Field field1 = new Field("field", text, TextField.TYPE_STORED);
        Field field2 = new Field("field", text2, TextField.TYPE_STORED);
        document1.add(field1);
        document2.add(field2);
        //将一行数据添加到xx中进行分析解析
        indexWriter.addDocument(document1);
        indexWriter.addDocument(document2);
        indexWriter.close();
        /*end region*/

        //打开索引
        DirectoryReader directoryReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(directoryReader);
        //搜索的基本单元，需要转换为TermQuery才能被识别。
        Term term = new Term("field", "text");
        //Lucene只能识别Query不能识别Term，所以通过此进行封装。
        Query query = new TermQuery(term);
        //检索出前1000条记录。
        TopDocs topDocs = indexSearcher.search(query, 1000);
        ScoreDoc[] scoreDoc = topDocs.scoreDocs;
        for (int i = 0; i < scoreDoc.length; i++) {

            Document hitDoc = indexSearcher.doc(scoreDoc[i].doc);
            System.out.println(hitDoc.get("field"));
        }

        // QueryParser parser=new QueryParser(Version.LUCENE_40,"field",analyzer);
        //Query query=parser.parse("text");
        ////第三个参数1000是检索得分最高的前1000条记录。
        //
        //ScoreDoc[] hits=indexSearcher.search(query, null, 1000).scoreDocs;
        //System.out.println(hits.length);
        //
        //for (int i = 0; i < hits.length; i++) {
        //    Document hitDoc = indexSearcher.doc(hits[i].doc);
        //    //assertEquals("This is the text to be indexed.", hitDoc.get("fieldname"));
        //    System.out.println(hitDoc.get("field"));
        //}
    }
}
